<?php
/**
 * Created by PhpStorm.
 * User: dotscreen
 * Date: 14/12/2017
 * Time: 10:16
 */

namespace App;


use Monolog\Logger;

trait LoggableInterface
{
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function log(string $toLog)
    {
        $this->logger->debug($toLog);
    }

}