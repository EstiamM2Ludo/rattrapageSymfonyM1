<?php
/**
 * Created by PhpStorm.
 * User: dotscreen
 * Date: 07/12/2017
 * Time: 16:13
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{
    /**
     * @Route("/")
     */
    public function index(){
        return $this->render('main/index.html.twig');
    }

}