<?php
/**
 * Created by PhpStorm.
 * User: dotscreen
 * Date: 12/12/2017
 * Time: 12:43
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 * @ORM\HasLifecycleCallbacks()
 */

class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="integer", options={"default"=0})
     */
    private $quantity;

    /**
     * @ORM\Column(type="boolean", options={"default"=true})
     */
    private $available;

    /**
     * Many Products has Many Categories.
     * @ORM\ManyToMany(targetEntity="App\Entity\Category",  inversedBy="products")
     * @ORM\JoinTable(name="products_categories")
     */
    private $categories;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function isAvailable()
    {
        return $this->available;
    }

    /**
     * @param mixed $available
     */
    public function setAvailable($available): void
    {
        $this->available = $available;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateAvailability()
    {
        //https://stackoverflow.com/questions/25607971/how-to-elegantly-log-inside-a-doctrine2-entity
        //echo "toto";
        //var_dump("toto");
        if($this->quantity<=0&&$this->available==true)
            $this->available =false;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __construct() {
        $this->categories = new ArrayCollection();
    }
}